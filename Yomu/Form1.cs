﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Collections.Specialized;
using System.Xml;

namespace Yomu
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        

        private void Form1_Load(object sender, EventArgs e)
        {

        }


        public void reload(bool cur)
        {

            if(cur)Cursor.Current = Cursors.WaitCursor;

            readList.Items.Clear();

            foreach (Image img in thumbs.Images)
            {
                img.Dispose();
            }
            thumbs.Images.Clear();

            

            ArrayList mangas = Librarian.GetBooks();

            foreach (BookInfo manga in mangas)
            {
                ListViewItem lvItem = new ListViewItem(manga.Title());
               
                    if (manga.HasThumbnail())
                    {
                        Bitmap tr = Tools.LoadImageToSize(manga.ThumbnailFullPath(), new Size(128, 128));
                        thumbs.Images.Add(tr);
                        tr.Dispose();
                        lvItem.ImageIndex = thumbs.Images.Count - 1;
                        GC.Collect();
                    }
                readList.Items.Add(lvItem);
                Application.DoEvents();
            }

            importList.Items.Clear();
            foreach (string ms in Librarian.GetImportables())
            {
                DirectoryInfo manga = new DirectoryInfo(ms);
                importList.Items.Add(manga.Name);
            }
            
            if(cur)Cursor.Current = Cursors.Default;
        }
        private void readList_ItemActivate(object sender, EventArgs e)
        {
            if (readList.SelectedIndices.Count == 1) nowRead();
        }
        private void nowRead()
        {
            BookInfo selection = Librarian.GetBook(readList.SelectedIndices[0]);
            if (selection.HasPassword())
            {
                string re = Microsoft.VisualBasic.Interaction.InputBox("Please enter password for reading " + selection.Title(), "Password", "", 0, 0);
                if (re != "")
                {
                    if (!selection.CheckPassword(re))
                    {
                        MessageBox.Show("Password incorrect!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                        return;
                    }
                }
                else return;
            }
            // begin reading here
            Cursor.Current = Cursors.WaitCursor;
            if (selection.Chapters().Count > 1)
            {
                ChapterList list = new ChapterList();
                list.currentBook = selection;
                list.Show();
            }
            else
            {
                Reader rdr = new Reader();
                rdr.thisChapter = ((ChapterInfo)selection.Chapters()[0]);
                rdr.Show();
            }
            Cursor.Current = Cursors.Default;
        }
        private void importList_SelectedIndexChanged(object sender, EventArgs e)
        {
            Importer i = new Importer();
            i.inPath = (string)importList.Items[importList.SelectedIndex];
            i.de = this;
            i.Show();

        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
            if (readList.SelectedIndices.Count == 1) nowRead();
        }

        private void menuItem2_Click(object sender, EventArgs e)
        {
            if (readList.SelectedIndices.Count != 1) return;
            BookInfo selection = Librarian.GetBook(readList.SelectedIndices[0]);
            if (selection.HasPassword())
            {
                string re = Microsoft.VisualBasic.Interaction.InputBox("Please enter CURRENT password for " + selection.Title(), "Password", "", 0, 0);
                if (re != "")
                {
                    if (!selection.CheckPassword(re))
                    {
                        MessageBox.Show("Password incorrect!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                        return;
                    }
                }
                else return;
            }

            selection.ChangePassword(Microsoft.VisualBasic.Interaction.InputBox("Please enter NEW password for " + selection.Title(), "Password", "", 0, 0));
        }

        private void menuItem5_Click(object sender, EventArgs e)
        {
            reload(true);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Closing(object sender, CancelEventArgs e)
        {
            Application.Exit();
        }

        private void menuItem4_Click(object sender, EventArgs e)
        {
            if (readList.SelectedIndices.Count != 1) return;
            int i = readList.SelectedIndices[0];
            BookInfo selection = Librarian.GetBook(i);
            if (MessageBox.Show("Are you sure you want to delete " + selection.Title() + " containing " + selection.Chapters().Count.ToString() + " chapters? This can't be undone.", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No) return;

            if (selection.HasPassword())
            {
                string re = Microsoft.VisualBasic.Interaction.InputBox("Please enter password to DELETE " + selection.Title(), "Password", "", 0, 0);
                if (re != "")
                {
                    if (!selection.CheckPassword(re))
                    {
                        MessageBox.Show("Password incorrect!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                        return;
                    }
                }
                else return;
            }
            // begin reading here
            if (MessageBox.Show("Are you REALLY sure you want to delete " + selection.Title() + " containing " + selection.Chapters().Count.ToString() + " chapters? THIS CANNOT BE UNDONE!", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button2) == DialogResult.No) return;
            Librarian.DeleteBook(i);
            this.reload(true);
        }

       

       
    }
}