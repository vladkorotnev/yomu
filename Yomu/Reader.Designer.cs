﻿namespace Yomu
{
    partial class Reader
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс  следует удалить; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.imgView = new System.Windows.Forms.PictureBox();
            this.contextMenu1 = new System.Windows.Forms.ContextMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuItem6 = new System.Windows.Forms.MenuItem();
            this.menuItem7 = new System.Windows.Forms.MenuItem();
            this.menuItem8 = new System.Windows.Forms.MenuItem();
            this.mnuPage = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.menuItem5 = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.menuItem9 = new System.Windows.Forms.MenuItem();
            this.menuItem10 = new System.Windows.Forms.MenuItem();
            this.menuItem17 = new System.Windows.Forms.MenuItem();
            this.menuItem12 = new System.Windows.Forms.MenuItem();
            this.menuItem11 = new System.Windows.Forms.MenuItem();
            this.menuItem13 = new System.Windows.Forms.MenuItem();
            this.menuItem15 = new System.Windows.Forms.MenuItem();
            this.menuItem14 = new System.Windows.Forms.MenuItem();
            this.menuItem16 = new System.Windows.Forms.MenuItem();
            this.lblIfo = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.thinBar1 = new Yomu.ThinBar();
            this.SuspendLayout();
            // 
            // imgView
            // 
            this.imgView.ContextMenu = this.contextMenu1;
            this.imgView.Location = new System.Drawing.Point(0, 0);
            this.imgView.Name = "imgView";
            this.imgView.Size = new System.Drawing.Size(240, 320);
            this.imgView.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgView.MouseMove += new System.Windows.Forms.MouseEventHandler(this.View_MouseMove);
            this.imgView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.View_MouseDown);
            this.imgView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.View_MouseUp);
            // 
            // contextMenu1
            // 
            this.contextMenu1.MenuItems.Add(this.menuItem1);
            this.contextMenu1.MenuItems.Add(this.menuItem2);
            this.contextMenu1.MenuItems.Add(this.menuItem6);
            this.contextMenu1.MenuItems.Add(this.menuItem3);
            this.contextMenu1.MenuItems.Add(this.menuItem5);
            this.contextMenu1.MenuItems.Add(this.menuItem4);
            this.contextMenu1.MenuItems.Add(this.menuItem9);
            // 
            // menuItem1
            // 
            this.menuItem1.Text = "Next";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Text = "Previous";
            this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
            // 
            // menuItem6
            // 
            this.menuItem6.MenuItems.Add(this.menuItem7);
            this.menuItem6.MenuItems.Add(this.menuItem8);
            this.menuItem6.MenuItems.Add(this.mnuPage);
            this.menuItem6.Text = "Go to...";
            // 
            // menuItem7
            // 
            this.menuItem7.Text = "First";
            this.menuItem7.Click += new System.EventHandler(this.menuItem7_Click);
            // 
            // menuItem8
            // 
            this.menuItem8.Text = "Last";
            this.menuItem8.Click += new System.EventHandler(this.menuItem8_Click);
            // 
            // mnuPage
            // 
            this.mnuPage.Text = "Page...";
            this.mnuPage.Click += new System.EventHandler(this.mnuPage_Click);
            // 
            // menuItem3
            // 
            this.menuItem3.Text = "-";
            // 
            // menuItem5
            // 
            this.menuItem5.Text = "Help";
            this.menuItem5.Click += new System.EventHandler(this.menuItem5_Click);
            // 
            // menuItem4
            // 
            this.menuItem4.Text = "Finish";
            this.menuItem4.Click += new System.EventHandler(this.menuItem4_Click);
            // 
            // menuItem9
            // 
            this.menuItem9.MenuItems.Add(this.menuItem10);
            this.menuItem9.MenuItems.Add(this.menuItem17);
            this.menuItem9.MenuItems.Add(this.menuItem12);
            this.menuItem9.MenuItems.Add(this.menuItem11);
            this.menuItem9.Text = "Delete current page";
            // 
            // menuItem10
            // 
            this.menuItem10.Enabled = false;
            this.menuItem10.Text = "Are you sure?";
            // 
            // menuItem17
            // 
            this.menuItem17.Text = "-";
            // 
            // menuItem12
            // 
            this.menuItem12.Text = "No";
            // 
            // menuItem11
            // 
            this.menuItem11.MenuItems.Add(this.menuItem13);
            this.menuItem11.MenuItems.Add(this.menuItem15);
            this.menuItem11.MenuItems.Add(this.menuItem14);
            this.menuItem11.MenuItems.Add(this.menuItem16);
            this.menuItem11.Text = "Yes";
            // 
            // menuItem13
            // 
            this.menuItem13.Enabled = false;
            this.menuItem13.Text = "Really sure?";
            // 
            // menuItem15
            // 
            this.menuItem15.Text = "-";
            // 
            // menuItem14
            // 
            this.menuItem14.Text = "Yes";
            this.menuItem14.Click += new System.EventHandler(this.menuItem14_Click);
            // 
            // menuItem16
            // 
            this.menuItem16.Text = "No";
            // 
            // lblIfo
            // 
            this.lblIfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblIfo.BackColor = System.Drawing.Color.Transparent;
            this.lblIfo.ContextMenu = this.contextMenu1;
            this.lblIfo.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.lblIfo.ForeColor = System.Drawing.Color.Red;
            this.lblIfo.Location = new System.Drawing.Point(0, 300);
            this.lblIfo.Name = "lblIfo";
            this.lblIfo.Size = new System.Drawing.Size(240, 20);
            this.lblIfo.Text = "0 of 0";
            this.lblIfo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblIfo.Visible = false;
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.ContextMenu = this.contextMenu1;
            this.progressBar.Location = new System.Drawing.Point(4, 4);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(233, 20);
            this.progressBar.Visible = false;
            // 
            // thinBar1
            // 
            this.thinBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.thinBar1.ForeColor = System.Drawing.Color.Cyan;
            this.thinBar1.Location = new System.Drawing.Point(0, 318);
            this.thinBar1.Name = "thinBar1";
            this.thinBar1.Size = new System.Drawing.Size(240, 2);
            this.thinBar1.TabIndex = 3;
            // 
            // Reader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.ContextMenu = this.contextMenu1;
            this.Controls.Add(this.thinBar1);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.lblIfo);
            this.Controls.Add(this.imgView);
            this.KeyPreview = true;
            this.Location = new System.Drawing.Point(0, 0);
            this.MinimizeBox = false;
            this.Name = "Reader";
            this.Text = "Reader";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Reader_Load);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.View_MouseUp);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.View_MouseDown);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.Reader_Closing);
            this.Resize += new System.EventHandler(this.Reader_Resize);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.View_MouseMove);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Reader_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox imgView;
        private System.Windows.Forms.ContextMenu contextMenu1;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem menuItem2;
        private System.Windows.Forms.MenuItem menuItem3;
        private System.Windows.Forms.MenuItem menuItem4;
        private System.Windows.Forms.MenuItem menuItem5;
        private System.Windows.Forms.Label lblIfo;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.MenuItem menuItem6;
        private System.Windows.Forms.MenuItem menuItem7;
        private System.Windows.Forms.MenuItem menuItem8;
        private System.Windows.Forms.MenuItem mnuPage;
        private System.Windows.Forms.MenuItem menuItem9;
        private System.Windows.Forms.MenuItem menuItem10;
        private System.Windows.Forms.MenuItem menuItem17;
        private System.Windows.Forms.MenuItem menuItem12;
        private System.Windows.Forms.MenuItem menuItem11;
        private System.Windows.Forms.MenuItem menuItem13;
        private System.Windows.Forms.MenuItem menuItem15;
        private System.Windows.Forms.MenuItem menuItem14;
        private System.Windows.Forms.MenuItem menuItem16;
        private ThinBar thinBar1;
    }
}