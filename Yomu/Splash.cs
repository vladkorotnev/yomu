﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OpenNETCF.ComponentModel;

namespace Yomu
{
    public partial class Splash : Form
    {
        public Splash()
        {
            InitializeComponent();
        }
        private Form1 f = new Form1();
        private void Splash_Activated(object sender, EventArgs e)
        {
            
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            this.idtmProgressBar1.Start();
            f.reload(false);
            f.Show();
            this.Hide();
        }

    }
}