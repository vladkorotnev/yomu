﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;
using OpenNETCF.Drawing.Imaging;
using System.IO;

namespace Yomu
{
    
    class Tools
    {
        const int PPN_UNATTENDEDMODE = 0x00000003;
        [DllImport("CoreDll.dll")]
        static extern bool PowerPolicyNotify(int dwMessage, int onOrOff);
        [DllImport("CoreDll.dll")]
        static extern void SystemIdleTimerReset();
        private static bool isSleepEnabled = true;
        private static Timer insomniaTimer;
        public static void SetSleepEnabled(bool canSleep)
        {
            if (canSleep == isSleepEnabled) return;

            if (!canSleep)
            {
                PowerPolicyNotify(PPN_UNATTENDEDMODE, 1);
                if (insomniaTimer != null)
                {
                    insomniaTimer.Enabled = false;
                    insomniaTimer.Dispose();
                }
                insomniaTimer = new Timer();
                insomniaTimer.Interval = 1000;
                insomniaTimer.Tick += new EventHandler(insomniaTimer_Tick);
                insomniaTimer.Enabled = true;
            }
            else
            {
                PowerPolicyNotify(PPN_UNATTENDEDMODE, 0);
                if (insomniaTimer == null) return;
                insomniaTimer.Enabled = false;
                insomniaTimer.Dispose();
            }

            isSleepEnabled = canSleep;
        }

        private static void insomniaTimer_Tick(object sender, EventArgs e)
        {
            SystemIdleTimerReset();
        }
        public static Bitmap ResizePicture(Bitmap image, Size maxSize)
        {
            if (image == null)
                throw new ArgumentNullException("image", "Null passed to ResizePictureToMaximum");

            if ((image.Width > maxSize.Width) || (image.Height > maxSize.Height))
            {
                Bitmap resizedImage = new Bitmap(maxSize.Width, maxSize.Height);

                using (Graphics graphics = Graphics.FromImage(resizedImage))
                {
                    graphics.Clear(Color.White);

                    float widthRatio = maxSize.Width / image.Width;
                    float heightRatio = maxSize.Height / image.Height;

                    int width = maxSize.Width;
                    int height = maxSize.Height;

                    if (widthRatio > heightRatio)
                    {
                        width = (int)Math.Ceiling(maxSize.Width * heightRatio);
                    }
                    else if (heightRatio > widthRatio)
                    {
                        height = (int)Math.Ceiling(maxSize.Height * widthRatio);
                    }

                    graphics.DrawImage(
                      image,
                      new Rectangle(0, 0, width, height),
                      new Rectangle(0, 0, image.Width, image.Height),
                      GraphicsUnit.Pixel);
                }
                return resizedImage;
            }

            return image;
        } 

        public static unsafe Bitmap LoadImageToSize(string image, Size size)
        {
            ImagingFactoryClass sas = new ImagingFactoryClass();
            IImage img;
            IBitmapImage res;
            sas.CreateImageFromFile(image, out img);
            sas.CreateBitmapFromImage(img, (uint)size.Width, (uint)size.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb, InterpolationHint.InterpolationHintBicubic, out res);
            img = null;
            Bitmap loaded=ImageUtils.IBitmapImageToBitmap(res);
            sas = null;
            res = null;
            return loaded;
        }
        public static bool IsImage(FileInfo file) {
            return Tools.IsImage(file.Name);
        }
        public static bool IsImage(string fname)
        {
            return (fname.ToLower().EndsWith(".jpg") || fname.ToLower().EndsWith(".jpeg") || fname.ToLower().EndsWith(".png")) && !fname.StartsWith("._");
        }
         private static unsafe bool IsGrayScale(Bitmap bmp, byte thresh)
        {
            
                using (var g = Graphics.FromImage(bmp))
                {
                    g.DrawImage(bmp, 0, 0);
                }

                var data = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, PixelFormat.Format32bppRgb);

                var pt = (int*)data.Scan0;
                var res = true;

                for (var i = 0; i < data.Height * data.Width; i++)
                {
                    var color = Color.FromArgb(pt[i]);
                    int dA = color.R - color.G;
                    int dB = color.G - color.B;
                    int dC = color.R - color.B;
                    dA = Math.Abs(dA);
                    dB = Math.Abs(dB);
                    dC = Math.Abs(dC);
                    if (!((dA <= thresh) && (dB <= thresh) && (dC <= thresh)))
                    {
                        res = false;
                        break;
                    }
                }

                bmp.UnlockBits(data);

                return res;
            }
   

}

      
}


  
