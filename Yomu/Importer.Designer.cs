﻿namespace Yomu
{
    partial class Importer
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс  следует удалить; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbAuthor = new System.Windows.Forms.TextBox();
            this.cbPwd = new System.Windows.Forms.CheckBox();
            this.tbPwd = new System.Windows.Forms.TextBox();
            this.lbThumb = new System.Windows.Forms.ListBox();
            this.pbThumb = new System.Windows.Forms.PictureBox();
            this.cbTitle = new System.Windows.Forms.ComboBox();
            this.nmChapter = new System.Windows.Forms.NumericUpDown();
            this.lbStatus = new System.Windows.Forms.Label();
            this.idtmProgressBar1 = new Yomu.IDTMProgressBar();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            // 
            // menuItem1
            // 
            this.menuItem1.Text = "Import";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(2, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 20);
            this.label1.Text = "Title:";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(2, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 20);
            this.label2.Text = "Author:";
            // 
            // tbAuthor
            // 
            this.tbAuthor.Location = new System.Drawing.Point(59, 31);
            this.tbAuthor.Name = "tbAuthor";
            this.tbAuthor.Size = new System.Drawing.Size(176, 21);
            this.tbAuthor.TabIndex = 3;
            // 
            // cbPwd
            // 
            this.cbPwd.Location = new System.Drawing.Point(2, 59);
            this.cbPwd.Name = "cbPwd";
            this.cbPwd.Size = new System.Drawing.Size(82, 20);
            this.cbPwd.TabIndex = 5;
            this.cbPwd.Text = "Password";
            // 
            // tbPwd
            // 
            this.tbPwd.Location = new System.Drawing.Point(90, 58);
            this.tbPwd.Name = "tbPwd";
            this.tbPwd.Size = new System.Drawing.Size(145, 21);
            this.tbPwd.TabIndex = 6;
            // 
            // lbThumb
            // 
            this.lbThumb.Items.Add("");
            this.lbThumb.Location = new System.Drawing.Point(90, 86);
            this.lbThumb.Name = "lbThumb";
            this.lbThumb.Size = new System.Drawing.Size(145, 100);
            this.lbThumb.TabIndex = 7;
            this.lbThumb.SelectedIndexChanged += new System.EventHandler(this.lbThumb_SelectedIndexChanged);
            // 
            // pbThumb
            // 
            this.pbThumb.Location = new System.Drawing.Point(7, 88);
            this.pbThumb.Name = "pbThumb";
            this.pbThumb.Size = new System.Drawing.Size(76, 97);
            // 
            // cbTitle
            // 
            this.cbTitle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cbTitle.Location = new System.Drawing.Point(59, 3);
            this.cbTitle.Name = "cbTitle";
            this.cbTitle.Size = new System.Drawing.Size(128, 22);
            this.cbTitle.TabIndex = 10;
            this.cbTitle.SelectedIndexChanged += new System.EventHandler(this.cbTitle_SelectedIndexChanged);
            this.cbTitle.TextChanged += new System.EventHandler(this.cbTitle_TextChanged);
            // 
            // nmChapter
            // 
            this.nmChapter.Location = new System.Drawing.Point(193, 3);
            this.nmChapter.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nmChapter.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nmChapter.Name = "nmChapter";
            this.nmChapter.Size = new System.Drawing.Size(42, 22);
            this.nmChapter.TabIndex = 11;
            this.nmChapter.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lbStatus
            // 
            this.lbStatus.Location = new System.Drawing.Point(7, 231);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(228, 20);
            this.lbStatus.Text = "Enter all data, then tap Import.";
            this.lbStatus.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // idtmProgressBar1
            // 
            this.idtmProgressBar1.Location = new System.Drawing.Point(8, 191);
            this.idtmProgressBar1.Name = "idtmProgressBar1";
            this.idtmProgressBar1.Size = new System.Drawing.Size(226, 28);
            this.idtmProgressBar1.TabIndex = 15;
            // 
            // Importer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.idtmProgressBar1);
            this.Controls.Add(this.lbStatus);
            this.Controls.Add(this.nmChapter);
            this.Controls.Add(this.cbTitle);
            this.Controls.Add(this.pbThumb);
            this.Controls.Add(this.lbThumb);
            this.Controls.Add(this.tbPwd);
            this.Controls.Add(this.cbPwd);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbAuthor);
            this.Controls.Add(this.label1);
            this.Menu = this.mainMenu1;
            this.Name = "Importer";
            this.Text = "Importer";
            this.Load += new System.EventHandler(this.Importer_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbAuthor;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.CheckBox cbPwd;
        private System.Windows.Forms.TextBox tbPwd;
        private System.Windows.Forms.ListBox lbThumb;
        private System.Windows.Forms.PictureBox pbThumb;
        private System.Windows.Forms.ComboBox cbTitle;
        private System.Windows.Forms.NumericUpDown nmChapter;
        private System.Windows.Forms.Label lbStatus;
        private IDTMProgressBar idtmProgressBar1;
    }
}