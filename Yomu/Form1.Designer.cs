﻿namespace Yomu
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс  следует удалить; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageRead = new System.Windows.Forms.TabPage();
            this.readList = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.contextMenu1 = new System.Windows.Forms.ContextMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.thumbs = new System.Windows.Forms.ImageList();
            this.tabPageImport = new System.Windows.Forms.TabPage();
            this.importList = new System.Windows.Forms.ListBox();
            this.tabPageInfo = new System.Windows.Forms.TabPage();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.dbgMenu = new System.Windows.Forms.ContextMenu();
            this.menuItem5 = new System.Windows.Forms.MenuItem();
            this.tabControl1.SuspendLayout();
            this.tabPageRead.SuspendLayout();
            this.tabPageImport.SuspendLayout();
            this.tabPageInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageRead);
            this.tabControl1.Controls.Add(this.tabPageImport);
            this.tabControl1.Controls.Add(this.tabPageInfo);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(240, 294);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPageRead
            // 
            this.tabPageRead.Controls.Add(this.readList);
            this.tabPageRead.Location = new System.Drawing.Point(0, 0);
            this.tabPageRead.Name = "tabPageRead";
            this.tabPageRead.Size = new System.Drawing.Size(240, 271);
            this.tabPageRead.Text = "Read";
            // 
            // readList
            // 
            this.readList.Columns.Add(this.columnHeader1);
            this.readList.Columns.Add(this.columnHeader2);
            this.readList.Columns.Add(this.columnHeader3);
            this.readList.ContextMenu = this.contextMenu1;
            this.readList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.readList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.readList.LargeImageList = this.thumbs;
            this.readList.Location = new System.Drawing.Point(0, 0);
            this.readList.Name = "readList";
            this.readList.Size = new System.Drawing.Size(240, 271);
            this.readList.SmallImageList = this.thumbs;
            this.readList.TabIndex = 0;
            this.readList.ItemActivate += new System.EventHandler(this.readList_ItemActivate);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Name";
            this.columnHeader1.Width = 60;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Author";
            this.columnHeader2.Width = 60;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Pages";
            this.columnHeader3.Width = 60;
            // 
            // contextMenu1
            // 
            this.contextMenu1.MenuItems.Add(this.menuItem1);
            this.contextMenu1.MenuItems.Add(this.menuItem2);
            this.contextMenu1.MenuItems.Add(this.menuItem3);
            this.contextMenu1.MenuItems.Add(this.menuItem4);
            // 
            // menuItem1
            // 
            this.menuItem1.Text = "Read";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Text = "Change password";
            this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
            // 
            // menuItem3
            // 
            this.menuItem3.Text = "-";
            // 
            // menuItem4
            // 
            this.menuItem4.Text = "Delete";
            this.menuItem4.Click += new System.EventHandler(this.menuItem4_Click);
            // 
            // thumbs
            // 
            this.thumbs.ImageSize = new System.Drawing.Size(96, 128);
            // 
            // tabPageImport
            // 
            this.tabPageImport.Controls.Add(this.importList);
            this.tabPageImport.Location = new System.Drawing.Point(0, 0);
            this.tabPageImport.Name = "tabPageImport";
            this.tabPageImport.Size = new System.Drawing.Size(232, 268);
            this.tabPageImport.Text = "Import";
            // 
            // importList
            // 
            this.importList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.importList.Location = new System.Drawing.Point(0, 0);
            this.importList.Name = "importList";
            this.importList.Size = new System.Drawing.Size(232, 268);
            this.importList.TabIndex = 0;
            this.importList.SelectedIndexChanged += new System.EventHandler(this.importList_SelectedIndexChanged);
            // 
            // tabPageInfo
            // 
            this.tabPageInfo.Controls.Add(this.textBox1);
            this.tabPageInfo.Location = new System.Drawing.Point(0, 0);
            this.tabPageInfo.Name = "tabPageInfo";
            this.tabPageInfo.Size = new System.Drawing.Size(240, 271);
            this.tabPageInfo.Text = "Info";
            // 
            // textBox1
            // 
            this.textBox1.ContextMenu = this.dbgMenu;
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Location = new System.Drawing.Point(0, 0);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(240, 271);
            this.textBox1.TabIndex = 2;
            this.textBox1.Text = "Yomu 0.2 for Windows Mobile\r\nby Akasaka Ryuunosuke, 2014\r\n\r\nhttp://software.vladk" +
                "orotnev.me\r\n\r\nIcon by Nikolay Verin\r\nhttp://ncrow.deviantart.com/\r\n\r\nGreetz to e" +
                "veryone on 4pda!";
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // dbgMenu
            // 
            this.dbgMenu.MenuItems.Add(this.menuItem5);
            // 
            // menuItem5
            // 
            this.menuItem5.Text = "Reload Data";
            this.menuItem5.Click += new System.EventHandler(this.menuItem5_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 294);
            this.Controls.Add(this.tabControl1);
            this.KeyPreview = true;
            this.Name = "Form1";
            this.Text = "Yomu";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.Form1_Closing);
            this.tabControl1.ResumeLayout(false);
            this.tabPageRead.ResumeLayout(false);
            this.tabPageImport.ResumeLayout(false);
            this.tabPageInfo.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageRead;
        private System.Windows.Forms.TabPage tabPageImport;
        private System.Windows.Forms.ListView readList;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ImageList thumbs;
        private System.Windows.Forms.ListBox importList;
        private System.Windows.Forms.TabPage tabPageInfo;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ContextMenu contextMenu1;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem menuItem2;
        private System.Windows.Forms.MenuItem menuItem3;
        private System.Windows.Forms.MenuItem menuItem4;
        private System.Windows.Forms.ContextMenu dbgMenu;
        private System.Windows.Forms.MenuItem menuItem5;
    }
}

