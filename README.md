# Yomu #

Yomu is a damn-simple PDA manga reader.

### Features ###

* List books
* Chapters within books
* Zoom
* Password protect books (no actual encryption)
* Books do not show up in image viewers

### System requirements ###

* Windows Mobile 2003+
* .NET CF 2.0+
* *recommended* VGA resolution screen

### Importing books ###

Put each chapter into the /import/ folder. 
Tap the chapter folder on the Import tab.
Enter the book name if it's the first time you're importing it, or select the name from drop-down if it's a new chapter of what you've already had.
If necessary, adjust the chapter number using the numeric input to the right of the title drop-down.
Optionally, enter the author's name.
Finally, select a cover picture from the list. You can see a preview beside the list box.
Then hit "Import" in the menu bar and the book/chapter will be imported.

### Password protection ###

If you didn't set the password on first chapter import, then tap and hold on a book to set or change the password for it.
Different books can have different passwords.

### Reading ###

Double tap a book to read it. A password may be required if you set that.
Pan the page with your stylus.
Use the joystick left-right to change pages, up-down to zoom.
Tap the screen or press select on joystick to bring up the HUD and the start menu bar.