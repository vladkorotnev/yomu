﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;
using System.IO;
using System.Xml;
using System.Reflection;

namespace Yomu
{

    public static class MobileConfiguration
    {



        public static NameValueCollection Settings;
     
       static MobileConfiguration()
        {
           string appPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
            string configFile = Path.Combine(appPath, "app.config");
     
           if (!File.Exists(configFile))
           {
               throw new FileNotFoundException(string.Format("Application configuration file '{0}' not found.", configFile));
           }
    
           XmlDocument xmlDocument = new XmlDocument();
           xmlDocument.Load(configFile);
           XmlNodeList nodeList = xmlDocument.GetElementsByTagName("appSettings");
           Settings = new NameValueCollection();
    
           foreach (XmlNode node in nodeList)
           {
               foreach (XmlNode key in node.ChildNodes)
               {
                   Settings.Add(key.Attributes["key"].Value, key.Attributes["value"].Value);
               }
           }
       }
       public static string appPath()
       {
         return   Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
       }
       public static void Update()
       {
           string appPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
           string configFile = Path.Combine(appPath, "app.config");
           XmlTextWriter tw = new XmlTextWriter(configFile, System.Text.UTF8Encoding.UTF8);
           tw.WriteStartDocument();
           tw.WriteStartElement("configuration");
           tw.WriteStartElement("appSettings");

           for (int i = 0; i < Settings.Count; ++i)
           {
               tw.WriteStartElement("add");
               tw.WriteStartAttribute("key", string.Empty);
               tw.WriteRaw(Settings.GetKey(i));
               tw.WriteEndAttribute();

               tw.WriteStartAttribute("value", string.Empty);
               tw.WriteRaw(Settings.Get(i));
               tw.WriteEndAttribute();
               tw.WriteEndElement();
           }

           tw.WriteEndElement();
           tw.WriteEndElement();

           tw.Close();
       }
    }
}
