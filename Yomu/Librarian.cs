﻿using System;

using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Collections;
using System.Collections.Specialized;
using System.Xml;
using System.Drawing;
using System.Diagnostics;
using OpenNETCF.Drawing;
using OpenNETCF.Drawing.Imaging;
using System.Windows.Forms;
namespace Yomu
{
    public  class CompareFiles:IComparer
{
	public  CompareFiles()
	{
		

	}


    #region IComparer Members


    public  int Compare(object x, object y)
    {
        int result = 0;
        if((x is FileInfo) && (y is FileInfo))
        {
            FileInfo X = (FileInfo)x;
            FileInfo Y = (FileInfo)y;


            result = X.Name.CompareTo(Y.Name);
        }
        return result;
    }


    #endregion
}
    public class BookInfo : NameValueCollection
    {
        public BookInfo() { }

       public BookInfo(string bookDirectory)
        {
            DirectoryInfo manga = new DirectoryInfo(bookDirectory);
            if (File.Exists(Path.Combine(manga.FullName, "manga.inf")))
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(Path.Combine(manga.FullName, "manga.inf"));
                XmlNodeList nodeList = xmlDocument.GetElementsByTagName("mdata");
               
                 foreach (XmlNode key in nodeList[0].ChildNodes)
                    {
                        this.Add(key.Attributes["key"].Value, key.Attributes["value"].Value);
                    }

                 if(!this["dirname"].Equals(manga.Name))this["dirname"]=manga.Name;
                this.chapters = new ArrayList();
                foreach (XmlNode chapter in xmlDocument.GetElementsByTagName("chapter"))
                {
                    chapters.Add(new ChapterInfo(chapter, this));
                }
                
            }
            
        }
       public BookInfo(string title, string author, string folder, string thumbnail, string password)
        {

            this["title"] = title;
            this["author"] = author;
            this["dirname"] = folder;
            this["thumb"] = thumbnail;
            this["lastchap"] = "0";
            if(password != "") this["password"] = password;
            this.chapters = new ArrayList();
            
        }
        public string Title() { return this["title"]; }
        public string Author() { return this["author"]; }
        public string Folder() { return this["dirname"]; }
        public bool HasThumbnail()
        {
            if (this.Thumbnail() != "")
                return File.Exists(this.ThumbnailFullPath());
            else return false;
        }
        public string Thumbnail() { return this["thumb"]; }
        public string FullPath() {  return Path.Combine(Librarian.GetBookDataDirectory(), this.Folder()); }
        public string ThumbnailFullPath() { return Path.Combine(this.FullPath(), this.Thumbnail()); }
        public int LastChapter() { return Convert.ToInt32(this["lastchap"]); }
        public void SetLastChapter(int chapter) { this["lastchap"] = chapter.ToString(); this.WriteDataFile(); }
        private ArrayList chapters;
        public ArrayList Chapters() { return this.chapters; }
        public ChapterInfo Chapter(int idx) { return (ChapterInfo) this.Chapters()[idx]; }
        //DOESNT WORK huh, IOException
        public void KillChapter(int idx) { Directory.Delete(this.Chapter(idx).FullPath(), true); this.chapters.RemoveAt(idx); }
        public void AddChapter(ChapterInfo chapter) { this.chapters.Add(chapter); }
        public bool CheckPassword(string response) { return (this["password"] == response); }
        public bool HasPassword() { return (this["password"] != "" && this["password"] != null); }
        public void ChangePassword(string newpassword) { this["password"] = newpassword; this.WriteDataFile(); }
        public int MaxChapterNo() { int max = -1; foreach (ChapterInfo chapter in this.Chapters()) { if (chapter.ChapterNo() > max) max = chapter.ChapterNo(); } return max; }
        public ChapterInfo ChapterAtNo(int chapterNo) { foreach (ChapterInfo chapter in this.Chapters()) { if (chapter.ChapterNo() == chapterNo) return chapter; } return null; }
        public int NextChapterFromNo(int chapterNo) { int minDist = this.MaxChapterNo()-chapterNo; foreach (ChapterInfo chapter in this.Chapters()) { if (chapter.ChapterNo() - chapterNo > 0 && chapter.ChapterNo() - chapterNo < minDist) minDist = chapter.ChapterNo() - chapterNo; } return chapterNo + minDist; }
        public void WriteDataFile()
        {
            XmlTextWriter w = new XmlTextWriter( Path.Combine(this.FullPath(), "manga.inf") , UTF8Encoding.UTF8);
            w.WriteStartDocument();
            w.WriteStartElement("manga");
            // Write meta data
            w.WriteStartElement("mdata");
            foreach (string key in this.AllKeys)
            {
                if (!key.Equals("chapters"))
                {
                    w.WriteStartElement("field");

                    w.WriteStartAttribute("key");
                    w.WriteRaw(key);
                    w.WriteEndAttribute();

                    w.WriteStartAttribute("value");
                    w.WriteRaw(this[key]);
                    w.WriteEndAttribute();

                    w.WriteEndElement();
                }
            }
            w.WriteEndElement();

            // Write chapters separately
            foreach (ChapterInfo chapter in this.Chapters())
            {
                w.WriteStartElement("chapter");

                foreach (string key in chapter.AllKeys)
                {
                    w.WriteStartElement("field");
                    w.WriteStartAttribute("key");
                    w.WriteRaw(key);
                    w.WriteEndAttribute();
                    w.WriteStartAttribute("value");
                    w.WriteRaw(chapter[key]);
                    w.WriteEndAttribute();
                    w.WriteEndElement();
                }

                w.WriteEndElement();

            }
            w.WriteEndElement();
            w.WriteEndDocument();
            w.Close();
        }
    }

   public class ChapterInfo : NameValueCollection
    {
        public ChapterInfo() { }
        public ChapterInfo(XmlNode chapterData, BookInfo sourceBook)
        {
            this.ParentBook = sourceBook;

            //this["title"] = sourceBook.Title();
            //this["author"] = sourceBook.Author();
            
            foreach (XmlNode key in chapterData.ChildNodes)
            {
                this.Add(key.Attributes["key"].Value, key.Attributes["value"].Value);
            }
        }
        public ChapterInfo(BookInfo book, string title, string author, string folder, string thumbnail, int number)
        {
            this["number"] = number.ToString();
            this["title"] = title;
            this["author"] = author;
            this["dirname"] = folder;
            this["thumb"] = thumbnail;
            this["last"] = "0";
            this.ParentBook = book;
        }
        public BookInfo ParentBook;

        public int ChapterNo() { return Convert.ToInt32(this["number"]); }
        public int LastPageIndex() { return Convert.ToInt32(this["last"]); }
        public void SetLastPageIndex(int index) { this["last"] = index.ToString(); this.ParentBook.WriteDataFile(); }
        public string Title() { return this["title"]; }
        public string Author() { return this["author"]; }
        public string Folder() { return this["dirname"]; }
        public bool HasThumbnail()
        {
            if (this.Thumbnail() != "")
                return File.Exists(this.ThumbnailFullPath());
            else return false;
        }
       private ArrayList absolutefiles = new ArrayList();
       public ArrayList AbsolutePagePaths() {
           if (this.absolutefiles.Count == 0)
           {
               DirectoryInfo dirinfo = new DirectoryInfo(this.FullPath());
               FileInfo[] sas = dirinfo.GetFiles("*"+Librarian.fnameExtz);
               ArrayList sus = new ArrayList(sas);
               sus.Sort(new CompareFiles());
               foreach (FileInfo file in sus) 
                   this.absolutefiles.Add(file.FullName);
           }
           return this.absolutefiles;
       }
       public string AbsolutePathForPageIndex(int index) { return this.AbsolutePagePaths()[index].ToString(); }
       public void DeletePageAtIndex(int index)
       {
           if (File.Exists(this.AbsolutePathForPageIndex(index))) File.Delete(this.AbsolutePathForPageIndex(index));
           this.absolutefiles.Clear();
       }
        public string Thumbnail() { return this["thumb"]; }
        public string FullPath() { return Path.Combine(ParentBook.FullPath(), this.Folder()); }
        public string ThumbnailFullPath() { return Path.Combine(this.FullPath(), this.Thumbnail()); }
    }

    public static class Librarian
    {
        public static string GetBookDataDirectory()
        {
            return Path.Combine(MobileConfiguration.appPath(), "bookdata");
        }

        public static string GetBookImportDirectory()
        {
            return Path.Combine(MobileConfiguration.appPath(), "import");
        }

      
        public static string[] GetImportables() {
           return Directory.GetDirectories(GetBookImportDirectory());
        }

        public static void CheckExistanceOfFolders()
        {
            if (!Directory.Exists(GetBookDataDirectory()))
                Directory.CreateDirectory(GetBookDataDirectory());
            if (!Directory.Exists(GetBookImportDirectory()))
                Directory.CreateDirectory(GetBookImportDirectory());
        }

        private static ArrayList books = new ArrayList();

        public static void Load()
        {
            CheckExistanceOfFolders();
            foreach (string ms in Directory.GetDirectories(GetBookDataDirectory()))
            {
                DirectoryInfo manga = new DirectoryInfo(ms);
                if (File.Exists(Path.Combine(manga.FullName, "manga.inf")))
                {
                    books.Add(new BookInfo(ms));
                }
            }
        }

        public static BookInfo FindBookByTitle(string strTitle)
        {
            foreach (BookInfo book in GetBooks())
            {
                if (book.Title().Equals(strTitle, StringComparison.InvariantCultureIgnoreCase)) return book;
            }
            return null;
        }
        public static readonly string fnameExtz = ".pge";

        public static void ImportBook(string impDirName, string impTitle, string impAuthor,string impThumbnail, bool setPassword, string strPassword, int chapNumber)
        {
            BookInfo book = FindBookByTitle(impTitle);
            impThumbnail += fnameExtz;
            bool wasFound = (book != null);
            if (!wasFound)
                book = new BookInfo(impTitle, impAuthor, impDirName, Path.Combine(impDirName+"_"+chapNumber.ToString(),impThumbnail), setPassword?strPassword:"");
            ChapterInfo chapter = new ChapterInfo(book, impTitle, impAuthor, book.Folder()+"_"+chapNumber.ToString(), impThumbnail, chapNumber);
           
            book.AddChapter(chapter);

            string srcPath = Path.Combine(GetBookImportDirectory(), impDirName);
            DirectoryInfo dir = new DirectoryInfo(srcPath);
            MakeDirectoriesForChapter(chapter);
            CopyFilesForChapter(dir, chapter);


            book.WriteDataFile();

            Directory.Delete(srcPath, true);

            if(!wasFound) books.Add(book);
            
        }

        private static void MakeDirectoriesForChapter(ChapterInfo chapter)
        {
            if (!Directory.Exists(chapter.ParentBook.FullPath())) Directory.CreateDirectory(chapter.ParentBook.FullPath());
            Directory.CreateDirectory(chapter.FullPath());
        }
        private static void CopyFilesForChapter(DirectoryInfo from, ChapterInfo to)
        {

            Tools.SetSleepEnabled(false);
            foreach (FileInfo f in from.GetFiles())
            {
                // TODO: Compact files
               
                if (Tools.IsImage(f))
                {
                    File.Move(f.FullName, Path.Combine(to.FullPath(), f.Name + fnameExtz));
                    Application.DoEvents();
                }
               
            }
            Tools.SetSleepEnabled(true);
        }

        public static ArrayList GetBooks()
        {
            if (books.Count == 0) Load();
            return books;
        }

        public static BookInfo GetBook(int bookIndex)
        {
            return (BookInfo) GetBooks()[bookIndex];
        }

        public static void DeleteBook(int bookIndex) {
            
            Directory.Delete( GetBook(bookIndex).FullPath(), true);
            books.RemoveAt(bookIndex);
        }
    }
}
