﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using OpenNETCF.Drawing.Imaging;
using OpenNETCF.Drawing;

namespace Yomu
{
    public partial class Importer : Form
    {
        public string inPath = "";
        public Form1 de;
        public Importer()
        {
            InitializeComponent();
        }

        private void Importer_Load(object sender, EventArgs e)
        {
            this.Text = Path.GetFileName(inPath);
            cbTitle.Text = this.Text;
            foreach(BookInfo book in Librarian.GetBooks()) 
                cbTitle.Items.Add(book.Title());
            lbThumb.SelectedIndex = 0;
            DirectoryInfo dirinf = new DirectoryInfo(Path.Combine(Librarian.GetBookImportDirectory(), inPath));
            foreach (FileInfo file in dirinf.GetFiles())
                if (Tools.IsImage(file))
                    lbThumb.Items.Add(file.Name);
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
           this.idtmProgressBar1.Start();
            lbStatus.Text = "Importing files...";
            this.Enabled = false;
            Librarian.ImportBook(this.inPath, cbTitle.Text, tbAuthor.Text, lbThumb.SelectedItem.ToString(), cbPwd.Checked, tbPwd.Text, Convert.ToInt32(nmChapter.Value));
            lbStatus.Text = "Reloading data...";
            de.reload(false);
            this.Close();
        }

        private void lbThumb_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (File.Exists(Path.Combine(Librarian.GetBookImportDirectory(), inPath + "\\" + lbThumb.SelectedItem)))
            {
                //Bitmap oelutz = new Bitmap(Path.Combine(Librarian.GetBookImportDirectory(), inPath + "\\" + lbThumb.SelectedItem));
               if(pbThumb.Image != null) pbThumb.Image.Dispose();

               pbThumb.Image = Tools.LoadImageToSize(Path.Combine(Librarian.GetBookImportDirectory(), inPath + "\\" + lbThumb.SelectedItem), pbThumb.Size);

                
               
            }
        }

        private void cbTitle_SelectedIndexChanged(object sender, EventArgs e)
        {
            // user is selecting an existing
            cbPwd.Enabled = false;
            tbPwd.Enabled = false;
            tbAuthor.Enabled = false;
            BookInfo book = Librarian.FindBookByTitle(cbTitle.Text);
            if (book != null)
            {
                nmChapter.Value = book.Chapters().Count + 1;
                tbAuthor.Text = book.Author();
            }
        }

        private void cbTitle_TextChanged(object sender, EventArgs e)
        {
            if(!cbTitle.Items.Contains(cbTitle.Text))
            {
                cbPwd.Enabled = true;
                   tbPwd.Enabled = true;
                   tbAuthor.Enabled = true;
            }
        }

   
    }
}