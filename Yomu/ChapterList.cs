﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Yomu
{
    public partial class ChapterList : Form
    {
        
        public ChapterList()
        {
            InitializeComponent();
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        public BookInfo currentBook;
        private void onLoad(object sender, EventArgs e)
        {
            this.Text = currentBook.Title();
            lblTitle.Text = currentBook.Author() + ": " + currentBook.Title();
            foreach (ChapterInfo chapter in currentBook.Chapters())
            {
                ListViewItem lvi = new ListViewItem("Chapter " + chapter.ChapterNo().ToString());
                if (chapter.HasThumbnail())
                {
                    Bitmap tr = Tools.LoadImageToSize(chapter.ThumbnailFullPath(), new Size(128, 128));
                    thumbs.Images.Add(tr);
                    tr.Dispose();
                    lvi.ImageIndex = thumbs.Images.Count - 1;
                }
                if (chapter.ChapterNo() == currentBook.LastChapter())
                {
                    lvi.BackColor = Color.DeepSkyBlue;
                }
                listView1.Items.Add(lvi);
            }
        }
        private void nowRead()
        {
            if (listView1.SelectedIndices.Count == 1)
            {
                Reader rdr = new Reader();
                rdr.chWindow = this;
                rdr.thisChapter = currentBook.Chapter(listView1.SelectedIndices[0]);
                rdr.Show();
            }
        }
        public void didFinishReading()
        {
            for (int i = 0; i < listView1.Items.Count - 1; i++)
            {
                ChapterInfo ch = currentBook.Chapter(i);
                ListViewItem lvi = listView1.Items[i];
                if (ch.ChapterNo() == currentBook.LastChapter())
                    lvi.BackColor = Color.DeepSkyBlue;
                else
                    lvi.BackColor = Color.White;
            }
        }
        private void listView1_ItemActivate(object sender, EventArgs e)
        {
            nowRead();
        }

        private void menuItem3_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count == 1)
            {
                ChapterInfo selection = currentBook.Chapter(listView1.SelectedIndices[0]);
                
                if (MessageBox.Show("Are you sure to delete chapter " + selection.ChapterNo().ToString() + " of " + currentBook.Title() + "?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No) return;
                if (MessageBox.Show("This cannot be undone!", "Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.Cancel) return;

                currentBook.KillChapter(listView1.SelectedIndices[0]);
               
                listView1.Items.Clear();
                foreach (Image i in thumbs.Images) i.Dispose();
                thumbs.Images.Clear();
                foreach (ChapterInfo chapter in currentBook.Chapters())
                {
                    ListViewItem lvi = new ListViewItem("Chapter " + chapter.ChapterNo().ToString());
                    if (chapter.HasThumbnail())
                    {
                        Bitmap tr = Tools.LoadImageToSize(chapter.ThumbnailFullPath(), new Size(128, 128));
                        thumbs.Images.Add(tr);
                        tr.Dispose();
                        lvi.ImageIndex = thumbs.Images.Count - 1;
                    }
                    if (chapter.ChapterNo() == currentBook.LastChapter())
                    {
                        lvi.BackColor = Color.DeepSkyBlue;
                    }
                    listView1.Items.Add(lvi);
                }
            }
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
            nowRead();
        }
    }
}