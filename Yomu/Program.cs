﻿using System;

using System.Collections.Generic;
using System.Windows.Forms;

namespace Yomu
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [MTAThread]
        static void Main()
        {
            Application.Run(new Splash());
        }
    }
}