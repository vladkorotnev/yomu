﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace Yomu
{
    public partial class Reader : Form
    {
        public ChapterInfo thisChapter;
        private int thisPageIdx;
        public Reader()
        {
            InitializeComponent();
        }

        

        private void Reader_Load(object sender, EventArgs e)
        {
            this.Text = thisChapter.Title();
            this.Width = Screen.PrimaryScreen.WorkingArea.Width;
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            progressBar.Maximum = thisChapter.AbsolutePagePaths().Count - 1;
            thinBar1.Maximum = progressBar.Maximum;
            thisPageIdx = thisChapter.LastPageIndex();
            Tools.SetSleepEnabled(false);
            if (thisChapter.HasThumbnail() && thisPageIdx == 0)
            {
                // Skip one page if the first page is the cover pic. You don't want to read it most likely.
                // If you do, you can return to it anytime.
                if (thisChapter.ThumbnailFullPath().Equals(thisChapter.AbsolutePathForPageIndex(thisPageIdx)) && (thisPageIdx + 1) < thisChapter.AbsolutePagePaths().Count - 1) thisPageIdx++;
            }
            loadPage();
          
        }
        bool fits = true;
        private void loadPage()
        {
            Cursor.Current = Cursors.WaitCursor;
            if (imgView.Image != null) imgView.Image.Dispose();
            try
            {
                Bitmap sas = new Bitmap(thisChapter.AbsolutePathForPageIndex(thisPageIdx));
                imgView.Image = sas;// Tools.ResizePicture(sas, imgView.Size);
                if (fits)
                    zoomFactor = ((float) ((float)this.Size.Width) / ((float)imgView.Image.Size.Width));
                
                imgView.Size = new Size((int)((float)imgView.Image.Size.Width * zoomFactor), (int)((float)imgView.Image.Size.Height * zoomFactor));
                //sas.Dispose();
            }
            catch (Exception e)
            {
                GC.Collect();
                imgView.Image = Tools.LoadImageToSize(thisChapter.AbsolutePagePaths()[thisPageIdx].ToString(), imgView.Size);
            }

            progressBar.Value = thisPageIdx;
            thinBar1.position = thisPageIdx;
            lblIfo.Text = (thisPageIdx + 1).ToString() + " of " + thisChapter.AbsolutePagePaths().Count;
            zoomFactor = (float)imgView.Size.Width / imgView.Image.Size.Width;
            
                imgView.Left = 0-imgView.Width - this.Width; imgView.Top = 0;
            
            
            Cursor.Current = Cursors.Default;
            GC.Collect();
            recalcBoundaries();
        }



        private int pX, pY;
        private float zoomFactor=1.0F;

        private void View_MouseDown(object sender, MouseEventArgs e)
        {
            Debug.WriteLine("mouse down");
            pX = e.X;
            pY = e.Y;
            thinBar1.Visible = false;
        }
        private void View_MouseMove(object sender, MouseEventArgs e)
        {
           
        }
        private void View_MouseUp(object sender, MouseEventArgs e)
        {
            Debug.WriteLine("mouse up");
            thinBar1.Visible = true;
            if (Math.Sqrt(Math.Pow( (e.X-pX),2) + Math.Pow((e.Y-pY),2)) <= 15)
            {
                // click
       
                toggle();
            }
            else
            {
                //move
                zoomFactor = (float)imgView.Size.Width / imgView.Image.Size.Width;
                imgView.Left += (e.X - pX);
                imgView.Top += (e.Y - pY);
                recalcBoundaries();
            }
        }
        private void recalcBoundaries()
        {
            if (imgView.Left < (-(imgView.Width - this.Width))) imgView.Left = -1 * (imgView.Width - this.Width);
            if (imgView.Left > 0) imgView.Left = 0;
            if (imgView.Top < -(imgView.Height - this.Height)) imgView.Top = -1 * (imgView.Height - this.Height);
            if (imgView.Top > 0) imgView.Top = 0;
        }
        private void next()
        {
            if (thisPageIdx == thisChapter.AbsolutePagePaths().Count - 1) return;
            thisPageIdx++;
            loadPage();
        }
        private void prev()
        {
            if (thisPageIdx == 0) return;
            thisPageIdx--;
            loadPage();
        }
        private void zoomIn()
        {
            zoomFactor = (float)imgView.Size.Width / imgView.Image.Size.Width;
            if (zoomFactor >= 2.0F) return;
            zoomFactor += 0.5F;
            fits = false;
            Size sas = new Size(Convert.ToInt32(this.Size.Width * zoomFactor),Convert.ToInt32(this.Size.Height * zoomFactor));
            imgView.Size = sas;
            loadPage();
        }
        private void zoomOut()
        {
            zoomFactor = (float)imgView.Size.Width / imgView.Image.Size.Width;
            if (zoomFactor <= ((float) ((float)this.Size.Width) / ((float)imgView.Image.Size.Width))) return;
            zoomFactor -= 0.5F;
            if (zoomFactor <= ((float)((float)this.Size.Width) / ((float)imgView.Image.Size.Width)))
            {
                zoomFactor = ((float)((float)this.Size.Width) / ((float)imgView.Image.Size.Width));
                fits = true;
            }
            else fits = false;
            Size sas = new Size(Convert.ToInt32(this.Size.Width * zoomFactor),Convert.ToInt32(this.Size.Height * zoomFactor));
            imgView.Size = sas;
            loadPage();
        }
        private void toggle()
        {
            if (this.WindowState == FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Normal;
                lblIfo.Visible = true;
                progressBar.Visible = true;
                progressBar.Value = thisPageIdx;
                lblIfo.Text = (thisPageIdx + 1).ToString() + " of " + thisChapter.AbsolutePagePaths().Count;
                thinBar1.Visible = false;
            }
            else
            {
                this.WindowState = FormWindowState.Maximized;
                lblIfo.Visible = false;
                progressBar.Visible = false;
                thinBar1.Visible = true;
            }
        }
        private void menuItem5_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Joystick: left-right flip pages, up-down change zoom, pan with stylus, center - info. ", "Help", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
            next();
        }

        private void menuItem2_Click(object sender, EventArgs e)
        {
            prev();
        }

        private void menuItem4_Click(object sender, EventArgs e)
        {
            if (imgView.Image != null) imgView.Image.Dispose();
            this.Close();
        }

        private void Reader_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == System.Windows.Forms.Keys.Up))
            {
                // Круглодисковая кнопка вверх
                // Вверх
                zoomIn();
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Down))
            {
                // Круглодисковая кнопка вниз
                // Вниз
                zoomOut();
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Left))
            {
                // Влево
                prev();
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Right))
            {
                // Вправо
                next();
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Enter))
            {
                // Ввод
                toggle();
            }

        }
        public ChapterList chWindow;
        private void Reader_Closing(object sender, CancelEventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (thisPageIdx == thisChapter.AbsolutePagePaths().Count - 1)
            {
                thisPageIdx = 0; // last page -- assume reading finished
                if (thisChapter.ParentBook.MaxChapterNo() > thisChapter.ChapterNo())
                {
                    thisChapter.ParentBook.SetLastChapter(thisChapter.ParentBook.NextChapterFromNo(thisChapter.ChapterNo()));
                }
                else thisChapter.ParentBook.SetLastChapter(-1);
            }
            else thisChapter.ParentBook.SetLastChapter(thisChapter.ChapterNo());
            thisChapter.SetLastPageIndex(thisPageIdx);
            if (chWindow != null) chWindow.didFinishReading();
            Tools.SetSleepEnabled(true);
            Cursor.Current = Cursors.Default;
        }

        private void menuItem7_Click(object sender, EventArgs e)
        {
            thisPageIdx = 0;
            loadPage();
        }

        private void menuItem8_Click(object sender, EventArgs e)
        {
            thisPageIdx = thisChapter.AbsolutePagePaths().Count - 1;
            loadPage();

        }

        private void mnuPage_Click(object sender, EventArgs e)
        {
            int input = Convert.ToInt32(Microsoft.VisualBasic.Interaction.InputBox("Page number", "Go to...", thisPageIdx.ToString(), 0, 0).Trim());
            if (input > 0 && input < thisChapter.AbsolutePagePaths().Count+1)
            {
                thisPageIdx = input-1;
                loadPage();
            } 
        }
        bool first = true;
        private void Reader_Resize(object sender, EventArgs e)
        {
            if (imgView.Image == null) return;
            if (first)
            {
                first = false; return;
            }
            if (fits)
                zoomFactor = ((float)((float)this.Size.Width) / ((float)imgView.Image.Size.Width));

            imgView.Size = new Size((int)((float)imgView.Image.Size.Width * zoomFactor), (int)((float)imgView.Image.Size.Height * zoomFactor));

            recalcBoundaries();
        }

        private void menuItem14_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Really sure to delete page " + (thisPageIdx + 1).ToString() + " of " + thisChapter.Title() + "? This can not be undone.", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                thisChapter.DeletePageAtIndex(thisPageIdx);
                if (thisPageIdx >= thisChapter.AbsolutePagePaths().Count) thisPageIdx = thisChapter.AbsolutePagePaths().Count - 1;
                loadPage();
            }
        }

        
    }
}